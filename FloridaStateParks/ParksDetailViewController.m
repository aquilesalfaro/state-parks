//
//  ParksDetailViewController.m
//  FloridaStateParks
//
//  Created by Aquiles Alfaro on 11/6/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "ParksDetailViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ParksDetailViewController () 
@property (weak, nonatomic) IBOutlet UILabel *lblParkName;
@property (weak, nonatomic) IBOutlet UIImageView *lblParkImage;
@property (weak, nonatomic) IBOutlet UILabel *lblParkLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblParkOpened;
@property (weak, nonatomic) IBOutlet UILabel *lblParkAddress;
@property (weak, nonatomic) IBOutlet MKMapView *mapPark;
@property (weak, nonatomic) IBOutlet UILabel *lblParkDescription;










@end

@implementation ParksDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = self.myPark.parkName;
    self.lblParkLocation.text = self.myPark.parkLocation;
    self.lblParkOpened.text = self.myPark.parkYearOpened;
    self.lblParkAddress.text = [NSString stringWithFormat:@"%@ \n%@ \n%@ ", self.myPark.parkAddress1, _myPark.parkAddress2, _myPark.parkAddress3];
    self.lblParkDescription.text = self.myPark.parkDescription;
    self.lblParkImage.image = self.myPark.parkImage;
    MKCoordinateRegion region = [self.mapPark regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(self.myPark.parkLong, self.myPark.parkLat), 60000, 60000)];
    [self.mapPark setRegion:region animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
