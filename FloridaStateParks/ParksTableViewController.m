//
//  ParksTableViewController.m
//  FloridaStateParks
//
//  Created by Aquiles Alfaro on 11/5/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "ParksTableViewController.h"
#import "parkTableViewCell.h"
#import "parks.h"
#import "ParksDetailViewController.h"

@interface ParksTableViewController ()

@property (strong, nonatomic)NSMutableArray *myParks;

@end

@implementation ParksTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    parks *bigCypress = [[parks alloc]init];
    bigCypress.parkName = @"Big Cypress";
    bigCypress.parkLocation = @"Ochopee, FL";
    bigCypress.parkImage = [UIImage imageNamed:@"bigCypress"];
    bigCypress.parkYearOpened = @"1973";
    bigCypress.parkAddress1 = @"52105 Tamiami Trail East";
    bigCypress.parkAddress2 = @"Ochopee, Florida 34141";
    bigCypress.parkAddress3 = @"Phone:  239-695-1201";
    bigCypress.parkDescription = @"The freshwaters of the Big Cypress Swamp, essential to the health of the neighboring Everglades, support the rich marine estuaries along Florida's southwest coast. Protecting over 729,000 acres of this vast swamp, Big Cypress National Preserve contains a mixture of tropical and temperate plant communities that are home to a diversity of wildlife, including the elusive Florida panther.";
    bigCypress.parkLong = 26.0485819;
    bigCypress.parkLat = -81.0776597;
    
    
    parks *biscayne = [[parks alloc]init];
    biscayne.parkName = @"Biscayne";
    biscayne.parkLocation = @"Key Biscayne, FL";
    biscayne.parkImage = [UIImage imageNamed:@"biscayne"];
    biscayne.parkYearOpened = @"1962";
    biscayne.parkAddress1 = @"9700 SW 328th Street";
    biscayne.parkAddress2 = @"Homestead, FL  33033";
    biscayne.parkAddress3 = @"Phone:  305-230-1144";
    biscayne.parkDescription = @"Within sight of downtown Miami, yet worlds away, Biscayne protects a rare combination of aquamarine waters, emerald islands, and fish-bejeweled coral reefs. Here too is evidence of 10,000 years of human history, from pirates and shipwrecks to pineapple farmers and presidents. Outdoors enthusiasts can boat, snorkel, camp, watch wildlife…or simply relax in a rocking chair gazing out over the bay.";
    biscayne.parkLong = 25.4541162;
    biscayne.parkLat = -80.3936556;

    
    parks *canaveral = [[parks alloc]init];
    canaveral.parkName = @"Canaveral";
    canaveral.parkLocation = @"New Smyrna Beach, FL";
    canaveral.parkImage = [UIImage imageNamed:@"canaveral"];
    canaveral.parkYearOpened = @"1987";
    canaveral.parkAddress1 = @"212 S. Washinton Ave.";
    canaveral.parkAddress2 = @"Titusville, FL  32796";
    canaveral.parkAddress3 = @"Phone: 321-267-1110";
    canaveral.parkDescription = @"Since ancient times, this barrier island has provided sanctuary to both people and wildlife. Many threatened and endangered species find refuge here, including sea turtles who nest on its shores. Like first natives and early settlers, you too can find tranquility. Stroll down a wooded trail. Reflect on a pristine undeveloped shoreline - the way things used to be.";
    canaveral.parkLong = 28.926937;
    canaveral.parkLat = -80.8269712;
    
    parks *castillo = [[parks alloc]init];
    castillo.parkName = @"Castillo de San Marcos";
    castillo.parkLocation = @"St. Augusting, FL";
    castillo.parkImage = [UIImage imageNamed:@"castillo"];
    castillo.parkYearOpened = @"1946";
    castillo.parkAddress1 = @"1 South Castillo Drive";
    castillo.parkAddress2 = @"Saint Augustine, FL  32084";
    castillo.parkAddress3 = @"Phone: 904-829-6506";
    castillo.parkDescription = @"A monument not only of stone and mortar but of human determination and endurance, the Castillo de San Marcos symbolizes the clash between cultures which ultimately resulted in our uniquely unified nation. Still resonant with the struggles of an earlier time, these original walls provide tangible evidence of America’s grim but remarkable history.";
    castillo.parkLong = 29.89732;
    castillo.parkLat = -81.3136895;
    
    parks * deSoto = [[parks alloc]init];
    deSoto.parkName = @"De Soto";
    deSoto.parkLocation = @"Bradenton, Fl";
    deSoto.parkImage = [UIImage imageNamed:@"desoto"];
    deSoto.parkYearOpened = @"1949";
    deSoto.parkAddress1 = @"8300 De Soto Memorial Hwy";
    deSoto.parkAddress2 = @"Bradenton, FL  34209";
    deSoto.parkAddress3 = @"Phone: 941-792-3428";
    deSoto.parkDescription = @"In May 1539, Conquistador Hernando de Soto’s army of soldiers, hired mercenaries, craftsmen and clergy made landfall in Tampa Bay. They were met with fierce resistance of indigenous people protecting their homelands. De Soto’s quest for glory and gold would be a four year, four thousand mile odyssey of intrigue, warfare, disease, and discovery that would form the history of the United States.";
    deSoto.parkLong = 27.5216976;
    deSoto.parkLat = -82.6456349;
    
    parks * dryTortugas = [[parks alloc]init];
    dryTortugas.parkName = @"Dry Tortugas";
    dryTortugas.parkLocation = @"Key West, FL";
    dryTortugas.parkImage = [UIImage imageNamed:@"dryTortugas"];
    dryTortugas.parkYearOpened = @"1946";
    dryTortugas.parkAddress1 = @"40001 SR-9336";
    dryTortugas.parkAddress2 = @"Homestead, FL  33034";
    dryTortugas.parkAddress3 = @"Phone: 305-242-7700";
    dryTortugas.parkDescription = @"Almost 70 miles (113 km) west of Key West lies the remote Dry Tortugas National Park. The 100-square mile park is mostly open water with seven small islands. Accessible only by boat or seaplane, the park is known the world over as the home of magnificent Fort Jefferson, picturesque blue waters, superlative coral reefs and marine life, and the vast assortment of bird life that frequent the area.";
    dryTortugas.parkLong = 24.6460508;
    dryTortugas.parkLat = -83.0064127;
    
    parks * everglades = [[parks alloc]init];
    everglades.parkName = @"Everglades";
    everglades.parkLocation = @"Miami, Naples, Homestead, FL";
    everglades.parkImage = [UIImage imageNamed:@"everglades"];
    everglades.parkYearOpened = @"1958";
    everglades.parkAddress1 = @"40001 State Road 9336";
    everglades.parkAddress2 = @"Homestead, FL  33034";
    everglades.parkAddress3 = @"Phone: 305-242-7700";
    everglades.parkDescription = @"Everglades National Park protects an unparalleled landscape that provides important habitat for numerous rare and endangered species like the manatee, American crocodile, and the elusive Florida panther. An international treasure as well - a World Heritage Site, International Biosphere Reserve, a Wetland of International Importance, and a specially protected area under the Cartagena Treaty.";
    everglades.parkLong = 25.3657345;
    everglades.parkLat = -82.0719135;
    
    parks * fortCaroline = [[parks alloc]init];
    fortCaroline.parkName = @"Fort Caroline";
    fortCaroline.parkLocation = @"Jacksonville, FL";
    fortCaroline.parkImage = [UIImage imageNamed:@"fortCaroline"];
    fortCaroline.parkYearOpened = @"1971";
    fortCaroline.parkAddress1 = @"12713 Fort Caroline Road";
    fortCaroline.parkAddress2 = @"Jacksonville, FL  32225";
    fortCaroline.parkAddress3 = @"Phone: 904-641-7155";
    fortCaroline.parkDescription = @"At the settlement of la Caroline, French settlers struggled for survival in a new world.  Many sought religious freedom in a new land, while others were soldiers or tradesmen starting a new life.  The climactic battles fought here between the French and Spanish marked the first time that European nations fought for control of lands in what is now the United States.  It would not be the last time. ";
    fortCaroline.parkLong = 30.4753486;
    fortCaroline.parkLat = -81.8245502;
    
    
    parks * fortMatanzas = [[parks alloc]init];
    fortMatanzas.parkName = @"Fort Matanzas";
    fortMatanzas.parkLocation = @"St. Augustine, FL";
    fortMatanzas.parkImage = [UIImage imageNamed:@"fortMatanzas"];
    fortMatanzas.parkYearOpened = @"1982";
    fortMatanzas.parkAddress1 = @"8635 AIA South";
    fortMatanzas.parkAddress2 = @"Saint Augusting, FL  32080";
    fortMatanzas.parkAddress3 = @"Phone: 904-471-0116";
    fortMatanzas.parkDescription = @"Coastal Florida was a major field of conflict as European nations fought for control in the New World. As part of this struggle, Fort Matanzas guarded St. Augustine’s southern river approach. The colonial wars are over, but the monument is still protecting—not just the historic fort, but also the wild barrier island and the plants and animals who survive there amidst a sea of modern development.";
    fortMatanzas.parkLong = 30.4753486;
    fortMatanzas.parkLat = -81.8245502;
    
    parks * gulfIslands = [[parks alloc]init];
    gulfIslands.parkName = @"Gulf Islands";
    gulfIslands.parkLocation = @"Gulf Breeze, FL";
    gulfIslands.parkImage = [UIImage imageNamed:@"gulfIslands"];
    gulfIslands.parkYearOpened = @"1984";
    gulfIslands.parkAddress1 = @"1801 Gulf Breeze Parkway";
    gulfIslands.parkAddress2 = @"Gulf Breeze, FL  32563";
    gulfIslands.parkAddress3 = @"Phone: 850-934-2600";
    gulfIslands.parkDescription = @"What is it that entices people to the sea? Poet John Masefield wrote, “I must go down to the seas again, for the call of the running tide is a wild call and a clear call that may not be denied.” Millions of visitors are drawn to the islands in the northern Gulf of Mexico for the white sandy beaches, the aquamarine waters, a boat ride, a camping spot, a tour of an old fort, or a place to fish.";
    gulfIslands.parkLong = 30.2976003;
    gulfIslands.parkLat = -87.9539039;
    
    parks * gullahGeechee = [[parks alloc]init];
    gullahGeechee.parkName = @"Gullah/Geechee";
    gullahGeechee.parkLocation = @"FL, GA, NC, SC";
    gullahGeechee.parkImage = [UIImage imageNamed:@"gullahGeechee"];
    gullahGeechee.parkYearOpened = @"1976";
    gullahGeechee.parkAddress1 = @"P.O. Box 1007";
    gullahGeechee.parkAddress2 = @"Johns Island, SC  29457";
    gullahGeechee.parkAddress3 = @"Phone: 843-818-4587";
    gullahGeechee.parkDescription = @"Designated by Congress in 2006, the Gullah Geechee Cultural Heritage Corridor extends from Wilmington, North Carolina in the north to Jacksonville, Florida in the south. It is home to one of America's most unique cultures, a tradition first shaped by captive Africans brought to the southern United States from West Africa and continued in later generations by their descendents.";
    gullahGeechee.parkLong = 30.4559467;
    gullahGeechee.parkLat = -83.7829128;
    
    parks * timucuan = [[parks alloc]init];
    timucuan.parkName = @"Timucuan";
    timucuan.parkLocation = @"Jacksonville, FL";
    timucuan.parkImage = [UIImage imageNamed:@"timucuan"];
    timucuan.parkYearOpened = @"1977";
    timucuan.parkAddress1 = @"12713 Fort Caroline Road";
    timucuan.parkAddress2 = @"Jacksonville, FL  32225";
    timucuan.parkAddress3 = @"Phone: 904-641-7155";
    timucuan.parkDescription = @"Visit one of the last unspoiled coastal wetlands on the Atlantic Coast. Discover 6,000 years of human history and experience the beauty of salt marshes, coastal dunes, and hardwood hammocks. The Timucuan Preserve includes Fort Caroline and Kingsley Plantation.";
    timucuan.parkLong = 30.4544578;
    timucuan.parkLat = -81.4520604;
    
    
    self.myParks = [NSMutableArray arrayWithObjects:bigCypress, biscayne, canaveral, castillo, deSoto, dryTortugas, everglades, fortCaroline, fortMatanzas, gulfIslands, gullahGeechee, timucuan, nil];
    
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.myParks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"parkTableViewCell";
    parkTableViewCell *cell = [tableView
                                dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.parkName.font = [cell.parkName.font fontWithSize:20];
    cell.parkLocation.font = [cell.parkLocation.font fontWithSize:14];
    
   parks *item = [self.myParks objectAtIndex:indexPath.row];
    
    
    // Configure the cell...
    
    if (indexPath.row % 2 == 0){
        cell.backgroundColor = [UIColor lightGrayColor];
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    cell.parkName.text = item.parkName;
    cell.parkLocation.text = item.parkLocation;
    cell.parkImage.image = item.parkImage;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"showDetails"]){
        ParksDetailViewController *detailVC = [segue destinationViewController];
        
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        
        parks* item = [self.myParks objectAtIndex:myIndexPath.row];
        
        detailVC.myPark = item;
        
    }
    
    
    
    
    
    
    
    
    
    
    
}


@end
