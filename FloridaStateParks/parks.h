//
//  parks.h
//  FloridaStateParks
//
//  Created by Aquiles Alfaro on 11/5/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>  // needed for UIImage


@interface parks : NSObject
@property (strong, nonatomic)NSString *parkName;
@property (strong, nonatomic)NSString *parkLocation;
@property (strong, nonatomic)UIImage *parkImage;
@property (strong, nonatomic)NSString *parkYearOpened;
@property (strong, nonatomic)NSString *parkAddress1;
@property (strong, nonatomic)NSString *parkAddress2;
@property (strong, nonatomic)NSString *parkAddress3;
@property (strong, nonatomic)NSString *parkDescription;
@property (strong, nonatomic)NSString *parkMap;
@property double parkLong;
@property double parkLat;



@end
