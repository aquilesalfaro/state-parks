//
//  AppDelegate.h
//  FloridaStateParks
//
//  Created by Aquiles Alfaro on 11/5/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

