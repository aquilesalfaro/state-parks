//
//  parkTableViewCell.h
//  FloridaStateParks
//
//  Created by Aquiles Alfaro on 11/5/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface parkTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *parkImage;
@property (weak, nonatomic) IBOutlet UILabel *parkName;
@property (weak, nonatomic) IBOutlet UILabel *parkLocation;

@end
