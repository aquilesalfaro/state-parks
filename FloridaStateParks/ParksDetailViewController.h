//
//  ParksDetailViewController.h
//  FloridaStateParks
//
//  Created by Aquiles Alfaro on 11/6/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "parks.h"


@interface ParksDetailViewController : UIViewController
@property (strong, nonatomic)parks* myPark;

@end
